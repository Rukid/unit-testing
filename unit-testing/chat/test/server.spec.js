const server = require('../server.js');
const config = require('../config/index.js');
const fs = require('fs-extra');
const assert = require('assert');
const { COPYFILE_EXCL } = fs.constants;
const request = require('request');
const path = require("path");

describe('GET request', () => {
	let app;
	before((done) => {
		app = server.listen(3333, () => {
			done();
		});
	});

	it('get запрос к http://localhost:3333/file.txt  => вернет file.txt', () => {
    fs.appendFileSync(path.resolve(__dirname, '../files/file.txt'), 'Hello content!');
		request('http://localhost:3333/file.txt', (err, res, body) => {
  		let content = fs.readFileSync(path.resolve(__dirname, '../files/file.txt'), 'utf8');
			assert.equal(content, body);
			fs.removeSync(path.resolve(__dirname, '../files/file.txt'));
		});
	});

	it('get запрос к http://localhost:3333  => вернет index.html', () => {
		request('http://localhost:3333', (err, res, body) => {
			let content = fs.readFileSync(path.resolve(__dirname, '../public/index.html'), 'utf8');
			assert.equal(content, body); // Print the error if one occurred
		});
	});

	// Написать тесты на
	// get запрос к http://localhost:3333  => вернет index.html
	// get запрос к http://localhost:3333/file.txt => вернет file.txt
	after((done) => {
		app.close(() => {
			done();
		});
	});

});
